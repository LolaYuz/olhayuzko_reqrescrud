Feature: Validate CRUD operations at Reqres

  Scenario Outline:
    Given user is able to create a new user with "<name>" and "<job>"
    And validate that status code is 201
    When user is get the response body with "<URL>"
    And validate that status code is 200
    When user is updating the name of the existing user
      | name | May Bee |
    And validate that status code is 200
    When user is deleting the existing user
    Then validate that status code is 204

    Examples: Reqres data
      | name     | job     | URL                         |
      | Lola Yuz | QA Lead | https://reqres.in/api/users |