package steps;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojo.CreateUser;
import pojo.UpdateUser;


import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.APIUtils.*;
import static utils.ConfigReader.*;

public class ReqresSteps {

    Response response;
    int actualUserId;

    @Given("user is able to create a new user with {string} and {string}")
    public void userIsAbleToCreateANewUserWithAnd(String name, String job) {
        CreateUser createUser = CreateUser.builder()
                .name(name).job(job).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createUser))
                .post(getProperty("ReqresURL") + "/api/users")
                .then().log().all().extract().response();

        actualUserId = Integer.parseInt(JsonPath.read(response.asString(), "id"));
    }

    @And("validate that status code is {int}")
    public void validateThatStatusCodeIs(int statusCode) {
        int actualStatusCode = response.getStatusCode();
        assertThat(
                "I am expecting status code: " + statusCode,
                actualStatusCode,
                is(statusCode));
    }

    @When("user is updating the name of the existing user")
    public void userIsUpdatingTheNameOfTheExistingUser(Map<String, String> data) {
        UpdateUser updateUser = UpdateUser.builder()
                .name(data.get("name")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(updateUser))
                .patch(getProperty("ReqresURL") + "/api/users/" + actualUserId)
                .then().log().all().extract().response();
    }


    @When("user is get the response body with {string}")
    public void userIsGetTheResponseBodyWith(String url) {
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(url + actualUserId)
                .then().log().all().extract().response();


    }


    @When("user is deleting the existing user")
    public void userIsDeletingTheExistingUser() {
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("ReqresURL") + "/api/users/" + actualUserId)
                .then().log().all().assertThat().statusCode(204).extract().response();
    }


}
