package pojo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 {
 "name": "morpheus",
 "job": "leader"
 }
 */

public class CreateUser {

    @Builder.Default
    private String name = "Tom Rider";
    @Builder.Default
    private String job = "teacher";

}